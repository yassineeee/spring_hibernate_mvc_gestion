package com.yassine.dao.impl;

import com.yassine.dao.ICategoryDao;
import com.yassine.dao.ILigneCommandeClientDao;
import com.yassine.entities.Category;
import com.yassine.entities.LigneCommandeClient;

public class LigneCommandeClientDaoImpl extends GenericDaoImpl<LigneCommandeClient> implements ILigneCommandeClientDao {

}
